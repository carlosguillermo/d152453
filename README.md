Que hará nuestra app:
    1- Seguridad: Tiene que ser segura desde el primer hasta el último extremo, por eso desarrollaremos un cifrado MD5 de al menos 60 parámetros

    2- Familiaridad: Tiene que ser una app muy simple y familiar, es importante que el usuario se sienta cómodo desde la primera ejecución

    3- Funciones especiales:
        3.1 Aquí estoy: Esta app tendrá una función para entrar a grupos públicos* dependiendo de nuestra ubicación, como por ejemplo, un Chat para un festival, un chat de una provincia, o simplemente un chat para conocer algo

        3.2 Contacto directo: Si alguien te contacta directamente desde un chat público, no tendrá por qué conocer tu número de teléfono, simplemente, en la primera ejecución, debes establecer un 'NickName', el cual se mostrará en tus chats públicos y contactos directos. Los chats directos expiran a la media hora de ser iniciados

        3.3 Intants Images: Las "Instant Images" serán unas imágenes que se podrán ver una sola vez, aunque se incluirán repeticiones**, que permitirán ver una Intant Image una vez más.  

        3.4 Modo de Viaje: Este modo se activa automáticamente cuando te encuentras en una red "Roaming", esto genera que no se descargue ningún tipo de contenido multimedia, para que tu factura no aumente

        3.5 Audios con caducidad: Puedes enviar audios que expiren en un determinado momento, o transcurrido un tiempo determinado

        3.6 